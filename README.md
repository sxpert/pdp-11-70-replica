# PDP-11-70 replica

Project to rebuild boards for a PDP-11/70 computer.
Boards should be 100% compatible with the original, and offer a way to use more modern components.
https://hackaday.io/project/184695-pdp-1170-replica

## Roadmap

### Step 1

 - [ ] build components in kicad for hex-height original boards
 - [ ] build components in kicad for quad-height original boards
 - [ ] build components in kicad for dual-height original boards
 - [ ] build a PUniBone from http://www.retrocmp.com/stories/punibone-pdp-11-processor-test-bed as a test-harness
 - [ ] foreach (board in the original schematics for KB11-C and FP11-C at bitsavers)
   - [ ] redo all original schematics in Kicad
   - [ ] create the board
   - [ ] scrouge money for board manufacturing
   - [ ] send it to pcb house
   - [ ] scrounge much more money for components
   - [ ] go to digikey / mouser / (...) / ebay for the components
   - [ ] solder it all (use sockets everywhere, much easier to test)
 - [ ] attempt to build a motherboard (how many layers this monster will be ? no idea) 
 - [ ] assemble it all
 - [ ] install some OS on the thing and play

### step 2

redo the same thing with euro-card 6U sized boards that fit in a standard rack ?

## License
This project is openhardware.
